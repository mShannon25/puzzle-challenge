// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  unsplash: {
    ACCESS_KEY: '8ebe32bed9cea058b48d64a6aab3bac410f7a978fdefc1424a3647d579379cee',
    API_URL: 'https://api.unsplash.com/photos/random/?client_id='
  }
};
