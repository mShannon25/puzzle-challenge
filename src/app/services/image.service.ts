import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { UnsplashImage } from '../image.model';


@Injectable()
export class ImageService {

  private API_URL: string = environment.unsplash.API_URL;
  private ACCESS_KEY: string = environment.unsplash.ACCESS_KEY;
  public image: UnsplashImage;
  public error: Error;

  constructor(private http: HttpClient) { }

  getNewImage(image?: string): Observable<UnsplashImage> {
    return this.http
      .get<UnsplashImage>(this.API_URL + this.ACCESS_KEY)
      .pipe(
        catchError(this.handleError<UnsplashImage>('getNewImage'))
      );
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (err: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(err); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed:\n` + err.error);
    //  alert(err.error + '\n Try Again Later!');
      document.getElementById('puzzle').innerHTML = err.error + '\n Try Again Later!';

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
