import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit, HostListener, Renderer2 } from '@angular/core';

import { ImageService } from './services/image.service';
import { UnsplashImage } from './image.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  public title = 'Magic Tile Puzzle';
  public directions = 'Click the tiles to solve the puzzle';
  public difficulty = 4;
  public totalTiles: number = this.difficulty * 2;
  public moveCount = 0;

  public isItemVisible = false;

  private pieceWidth: number;
  private pieceHeight: number;
  private puzzleWidth: number;
  private puzzleHeight: number;

  // Storage
  private pieces = Array<any>();
  private mouse = { x: 0, y: 0 };
  private currPiece: Object;
  private emptyPiece: Object;
  private rightPiece: object;
  private leftPiece: object;
  private topPiece: object;
  private bottomPiece: object;

  private startPuzzle: Object;

  // Image stuff
  public newImage;
  public image: UnsplashImage;
  public puzzleImage;
  private puzzleStarted = false;
  public imageTitle = '';
  public userPort = '';
  public userName = '';

  // Canvas Element
  @ViewChild('puzzlecanvas') canvas: ElementRef;
  private ctx: CanvasRenderingContext2D;

  // Listeners
  @HostListener('document:click', ['$event'])
  onClick(evt) {
    console.log(evt);
    if (!this.puzzleStarted) {
      this.shufflePuzzle();
    } else {
      this.selectTile(evt);
      this.moveCount += 1;
      console.log(this.pieces);
    }
  }

  constructor(private imageService: ImageService, private renderer: Renderer2) { }

  /**
 * INIT call get image from unsplash API
 *
 * @returns {void}
 */
  ngOnInit(): void {
    this.getImageFromService();
  }

    /**
 * INIT get image from unsplash API
 * via image.service
 *
 * @returns {void}
 */
private getImageFromService(): void {
  this.newImage = this.imageService.getNewImage()
  .subscribe(image => {
    this.image = image;
    this.loadImage(this.image.urls['small']);
  });
}
  /**
* Load Image
* After loaded complete set piece width and height
* and puzzle width & height
*
* @param {string} imgSrc Image url from unsplash service.
* @returns {void}
*
*/
  private loadImage(imgSrc: string): void {
    const image = new Image();
    this.puzzleImage = image;
    image.addEventListener('load', evt => {
      this.pieceWidth = Math.floor(image.width / this.difficulty);
      this.pieceHeight = Math.floor(image.height / this.difficulty);
      this.puzzleWidth = this.pieceWidth * this.difficulty;
      this.puzzleHeight = this.pieceHeight * this.difficulty;

      this.imageTitle = this.image.location ? this.image.location['title'] : '';
      this.userPort = this.image.user ? this.image.user['portfolio_url'] : '';
      this.userName = this.image.user ? this.image.user['name'] : '';


      this.createCanvas();
      this.createPuzzleWithImage(image);
    });
    image.src = imgSrc;
  }

  /**
  * Create the canvas
  * based on puzzle width & height
  *
  * @returns {void}
  *
  */
  private createCanvas(): void {
    const canvasEle: HTMLCanvasElement = this.canvas.nativeElement;
    this.ctx = canvasEle.getContext('2d');
    this.canvas.nativeElement.width = this.puzzleWidth;
    this.canvas.nativeElement.height = this.puzzleHeight;
  }

  /**
* Create the puzzle
* with the loaded image
*
* @param {HTMLImageElement} img loaded Image
* @returns {void}
*
*/
  public createPuzzleWithImage(img: HTMLImageElement): void {
    this.ctx.drawImage(img, 0, 0, this.puzzleWidth, this.puzzleHeight, 0, 0, this.puzzleWidth, this.puzzleHeight);
    this.createOverlay('Click to Start');
    this.createPieces();
  }

  /**
  * Directions for game
  * based on puzzle width & height
  *
  * @param {string} msg Message for the user.
  * @returns {void}
  *
  */
  private createOverlay(msg: string): void {
    this.ctx.fillStyle = '#000000';
    this.ctx.globalAlpha = .5;
    this.ctx.fillRect(0, 0, this.puzzleWidth, this.puzzleHeight);
    this.ctx.fillStyle = '#FFFFFF';
    this.ctx.globalAlpha = 1;
    this.ctx.textAlign = 'center';
    this.ctx.textBaseline = 'middle';
    this.ctx.font = '25px Helvetica';
    this.ctx.fillText(msg, this.puzzleWidth / 2, this.puzzleHeight / 2);
  }

  /**
    * Turn image to puzzle pieces(object)
    * set last piece to empty
    * store them in array
    *
    * @param {string} msg Message for the user.
    * @returns {void}
    *
    */
  private createPieces(): void {
    let piece;
    let xPos = 0;
    let yPos = 0;
    const lastPiece = (this.difficulty * this.difficulty) - 1;
    for (let i = 0; i < (this.difficulty * this.difficulty); i++) {
      piece = {};
      piece.startX = xPos;
      piece.startY = yPos;
      // Set last piece to empty
      if (i === lastPiece) {
        piece.empty = true;
        // store last piece
        this.emptyPiece = piece;
      } else {
        piece.empty = false;
      }
      this.pieces.push(piece);
      xPos += this.pieceWidth;
      if (xPos >= this.puzzleWidth) {
        xPos = 0;
        yPos += this.pieceHeight;
      }
      console.log(piece);
    }
  }
  /**
    * ES6 Shuffle array
    * thank you web
    *
    * @param {array} msg array of pieces(object).
    * @returns {Array} shuffled pieces
    *
    */
  private shuffleArr(array): Array<any> {
    const shuffled: Array<any> = array
      .map((a) => ({ sort: Math.random(), value: a }))
      .sort((a, b) => a.sort - b.sort)
      .map((a) => a.value);
    return shuffled;
  }

  /**
  * Draw out the shuffled pieces
  * // TODO: this is a mess, move the draw canvas out at least
  *
  * @returns {void}
  *
  */
  public shufflePuzzle(shuffle: boolean = true): void {
    this.pieces = this.shuffleArr(this.pieces);
    this.ctx.clearRect(0, 0, this.puzzleWidth, this.puzzleHeight);
    this.puzzleStarted = true;
    this.mouse = { x: 0, y: 0 };
    let piece;
    let xPos = 0;
    let yPos = 0;
    for (let i = 0; i < this.pieces.length; i++) {
      piece = this.pieces[i];
      piece.xPos = xPos;
      piece.yPos = yPos;
      this.ctx.drawImage(
        this.puzzleImage,
        piece.startX,
        piece.startY,
        this.pieceWidth,
        this.pieceHeight,
        xPos, yPos,
        this.pieceWidth,
        this.pieceHeight
      );
      this.ctx.strokeRect(xPos, yPos, this.pieceWidth, this.pieceHeight);
      if (piece.empty) {
        this.ctx.fillStyle = '#FFF';
        this.ctx.fillRect(xPos, yPos, this.pieceWidth, this.pieceHeight);
      }
      xPos += this.pieceWidth;
      if (xPos >= this.puzzleWidth) {
        xPos = 0;
        yPos += this.pieceHeight;
      }
    }
    console.log(this.pieces);
  }

  /**
  * User clicks on a tile
  * gets the x/y and calls getTile
  * then checks if the tile is movable
  *
  * @param {MouseEvent} evt click event.
  * @returns {void}
  *
  */
  private selectTile(evt: MouseEvent): void {
    console.log(evt.layerX + ' - ' + evt.layerY);
    // this.getTile(evt.layerX, evt.layerY);
    this.mouse.x = evt.layerX;
    this.mouse.y = evt.layerY;
    this.currPiece = this.getTile(evt.layerX, evt.layerY);
    this.leftPiece = this.getTile(evt.layerX - this.pieceWidth, evt.layerY);
    this.rightPiece = this.getTile(evt.layerX + this.pieceWidth, evt.layerY);
    this.topPiece = this.getTile(evt.layerX, evt.layerY - this.pieceHeight);
    this.bottomPiece = this.getTile(evt.layerX, evt.layerY + this.pieceHeight);

    // console.log(this.currPiece);
    // console.log(this.rightPiece);
    // console.log(this.leftPiece);

    this.canPieceMove(this.currPiece);

  }

  /**
  * Get the tile for the coordinates provided
  *
  * @param {number} xLayer mouse click x.
  * @param {number} yLayer mouse click y.
  * @returns {object} tile from the provided/selected x/y
  *
  */
  private getTile(xLayer: number, yLayer: number): object {
    let tile;
    // console.log('Tile layerX ' + xLayer + ' layerY ' + yLayer);
    for (let i = 0; i < this.pieces.length; i++) {
      tile = this.pieces[i];
      if (
        xLayer < tile.xPos ||
        xLayer > (tile.xPos + this.pieceWidth) ||
        yLayer < tile.yPos ||
        yLayer > (tile.yPos + this.pieceHeight)) {
      } else {
        // this.canPieceMove(piece, i);
        return tile;
      }
    }
    return null;
  }

  /**
  * Check to see if there is an open tile next
  * to the selected tile
  * above/below/left/right
  *
  * @param {string} msg Message for the user.
  * @returns {boolean} if true move the tile to the empty position
  *
  */
  private canPieceMove(selectedPiece): boolean {
    const leftPieceEmpty: boolean = (this.leftPiece) ? this.leftPiece['empty'] : false;
    const rightPieceEmpty: boolean = (this.rightPiece) ? this.rightPiece['empty'] : false;
    const topPieceEmpty: boolean = (this.topPiece) ? this.topPiece['empty'] : false;
    const bottomPieceEmpty: boolean = (this.bottomPiece) ? this.bottomPiece['empty'] : false;

    console.log('left: ' + leftPieceEmpty + '\nright: ' + rightPieceEmpty);
    // console.log('top: ' + topPieceEmpty + '\nbottom: ' + bottomPieceEmpty);

    switch (true) {
      case leftPieceEmpty: {
        // this.ctx.save();
        this.moveSetTile(this.leftPiece);
        break;
      }
      case rightPieceEmpty: {
        this.moveSetTile(this.rightPiece);
        break;
      }
      case topPieceEmpty: {
        this.moveSetTile(this.topPiece);
        break;
      }
      case bottomPieceEmpty: {
        this.moveSetTile(this.bottomPiece);
        break;
      }
      default: {
        console.log('Not Selectable');
        return false;
      }
    }
    return true;
  }

  /**
  * Change the position of the tile in the array
  * swap the xPos or yPos of the last and current tiles
  *
  * @param {object} newTile moved tile.
  * @returns {void}
  *
  */
  private moveSetTile(newTile): void {

    const xNum: number = this.currPiece['xPos'] / this.pieceWidth;
    const yNum: number = this.currPiece['yPos'] / this.pieceHeight;
    const index: number = this.difficulty * yNum + xNum;

    const newX: number = newTile.xPos / this.pieceWidth;
    const newY: number = newTile.yPos / this.pieceHeight;
    const newIndex: number = this.difficulty * newY + newX;

    console.log('Last Index ' + index);
    console.log('New Index ' + newIndex);

    const tempX = this.currPiece['xPos'];
    const tempY = this.currPiece['yPos'];
    const lastPiece = this.pieces[index];
    const nextPiece = this.pieces[newIndex];

    this.pieces.splice(newIndex, 1, lastPiece);
    this.pieces.splice(index, 1, nextPiece);

    console.log(this.pieces[newIndex].empty + ' = ' + newTile.xPos);
    console.log(this.pieces[index].empty + ' = ' + tempX);

    // X
    this.pieces[newIndex].xPos = newTile.xPos;
    this.pieces[index].xPos = tempX;
    // Y
    this.pieces[newIndex].yPos = newTile.yPos;
    this.pieces[index].yPos = tempY;

    this.redrawPuzzle();
    //  this.ctx.save();
  }

  /**
  * Clear the current empty piece and
  * redraw the canvas
  *
  * @returns {void}
  *
  */
  public redrawPuzzle(): void {
    console.log('Start Redraw');
    this.ctx.clearRect(0, 0, this.puzzleWidth, this.puzzleHeight);
    for (let i = 0; i < this.pieces.length; i++) {
      const piece = this.pieces[i];
      this.ctx.clearRect(piece.xPos, piece.yPos, this.pieceWidth, this.pieceHeight);
      this.ctx.drawImage(
        this.puzzleImage,
        piece.startX,
        piece.startY,
        this.pieceWidth,
        this.pieceHeight,
        piece.xPos, piece.yPos,
        this.pieceWidth,
        this.pieceHeight
      );
      if (piece.empty) {
        this.ctx.fillStyle = '#FFF';
        this.ctx.fillRect(piece.xPos, piece.yPos, this.pieceWidth, this.pieceHeight);
      }
      this.ctx.strokeRect(piece.xPos, piece.yPos, this.pieceWidth, this.pieceHeight);
    }
    // Check completed status
    const puzzStat = this.puzzleStatus();
    console.log('Puzzle Status ' + puzzStat);
  }

  /**
  * TEMP
  * Check for completion
  * //TODO: Not efficient but okay for now
  * @returns {void}
  *
  */
  private puzzleStatus(): boolean {
    for (let i = 0; i < this.difficulty * 2; i++) {
      const piece = this.pieces[i];
      if (piece.startX !== piece.xPos || piece.startY !== piece.yPos) {
        return false;
      }
    }
    this.createOverlay('Congrats, you solved the puzzle!');
    this.puzzleStarted = false;
    return true;
  }

  openUrl(): void {
    window.open('https://bitbucket.org/mShannon25/puzzle-challenge/src/master/', '_blank');
  }

  ngOnDestroy(): void {
    console.log('Unsubscribe Complete');
    this.newImage.unsubscribe();
  }
}
