export class UnsplashImage {
    constructor(
        public collections: string,
        public featured: string,
        public username: string,
        public query: string,
        public w: number,
        public h: number,
        public orientation: string,
        public count: string,
        public user: Array<any>[],
        public urls: Array<any>[],
        public location: Array<any>[]
    ) { }
}
